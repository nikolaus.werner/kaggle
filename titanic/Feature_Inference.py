# infered features (for NANs) 
# Fare has only one NaN --> its ok to infere like this
all_data['Fare'] = all_data['Fare']
all_data['Fare'].fillna(all_data.groupby(['Sex','Pclass','Title'])['Fare'].transform('median'),inplace=True)

all_data['Age_median'] = all_data['Age']
all_data['Age_median'].fillna(all_data.groupby(['Sex','Pclass','Title'])['Age'].transform('median'),inplace=True)

all_data = pd.get_dummies(all_data)

# infere Age via regression 
regression_variables = [
    'Sex_male',
    'Sex_female',
    'Pclass',
    'Title_Mr',
    'Title_Mrs',
    'Title_Ms',
    'Title_Rare',
    'Fare']
X_regression = all_data[~all_data['Age'].isnull()][regression_variables]
Y_regression = all_data[~all_data['Age'].isnull()]['Age']

parameters = {'fit_intercept':[True,False], 'normalize':[True,False], 'copy_X':[True, False]}
grid = GridSearchCV(LinearRegression(), parameters, cv=10)
grid.fit(X_regression, Y_regression)
print("r2 / variance : ", grid.best_score_)
lin_regression = grid.best_estimator_

lin_regression.fit(X_regression.values, Y_regression.values)

X_predict_age = all_data[all_data['Age'].isnull()][regression_variables]

all_data['Age_predicted'] = all_data['Age']
all_data.loc[all_data['Age_predicted'].isnull(), 'Age_predicted'] = lin_regression.predict(X_predict_age)

# separate data again
train_for_joining = train_raw.drop(['Age','Fare','Sex','Embarked','SibSp','Parch','Pclass','Survived'], axis=1)
test_for_joining = test_raw.drop(['Age','Fare','Sex','Embarked','SibSp','Parch','Pclass'], axis=1)

train = pd.merge(train_for_joining, all_data, on='PassengerId', how='inner')
train.drop(train[(train['Fare']> 100)].index,  inplace= True)

test = pd.merge(test_for_joining, all_data, on='PassengerId', how='inner')
