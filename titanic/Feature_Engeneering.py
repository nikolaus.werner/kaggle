# load 
train_raw = pd.read_csv('data/train.csv')
test_raw = pd.read_csv('data/test.csv')

# drop useless features
train_raw = train_raw.drop('Ticket', axis=1)
test_raw = test_raw.drop('Ticket', axis=1)

# aggregate
all_data = pd.concat([train_raw, test_raw])

# some features we might need 
all_data['Has_Cabin'] = all_data['Cabin'].apply(lambda x: 0 if type(x) == float else 1)

all_data['Family_Size'] = all_data['SibSp'] + all_data['Parch'] + 1

all_data['Embarked'] = all_data['Embarked'].fillna('U')

all_data['Cabin'] = all_data['Cabin'].fillna('X')
all_data['Deck'] = all_data.apply(lambda row: str(row['Cabin'])[0] if row['Cabin']!='T' else 'X',axis=1)

all_data = all_data.drop('Cabin',axis=1 )

# drop when not needed any more
train_raw = train_raw.drop('Cabin', axis=1)
test_raw = test_raw.drop('Cabin', axis=1)

# extract title information for later usage
all_data['Title'] = all_data['Name'].str.extract('(\w+)\.', expand=False)
all_data['Title'] = all_data['Title'].map({
    # women
    'Miss':'Ms',# unmarried
    'Ms' : 'Ms',
    'Mlle':'Ms',
    'Mrs':'Mrs', # married
    'Dona':'Mrs', 
    'Lady':'Mrs',
    'Mme':'Mrs',
    'Countess':'Mrs',
    # men
    'Mr':'Mr', 
    'Master':'Mr', 
    'Don':'Mr',
    'Sir':'Mr',
    # titles
    'Jonkheer':'Rare',
    'Rev':'Rare',
    'Dr':'Rare', 
    'Col':'Rare',
    'Capt':'Rare',
    'Major':'Rare',
     })
all_data = all_data.drop('Name', axis=1)
train_raw = train_raw.drop('Name', axis=1)
test_raw = test_raw.drop('Name', axis=1)

